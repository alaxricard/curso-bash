#!/usr/bin/env bash

clear
echo "------------------------------------------------"
echo -n "Autor: Alax Ricard"
echo ""
echo -n "Versão: 1.0"
echo ""
echo "------------------------------------------------"

echo "INFORMAÇÕES IMPORTANTES"
echo "------------------------------------------------"
echo -n "Usuário: "
whoami
echo -n "Hostname: "
hostname
echo -n "Tempo de uso: "
uptime -p
echo -n "Kernel: "
uname -rms
echo -n "Usuário logado: "
logname
echo  "-----------------------------------------------"
